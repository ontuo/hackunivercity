#define LIGHT_SENSOR  A0
#define TEMP_SENSOR A1
int light_value = 0, temp_value = 0;
String info_string = "";

void setup() {
  Serial.begin(9600);
}

void loop() {
  light_value = analogRead(LIGHT_SENSOR);
  temp_value = analogRead(TEMP_SENSOR);
  info_string = String(light_value) + "_" + String(temp_value) + "_";
  Serial.println(info_string);
  delay(3000);
}
