#! usr/bin/python3

import paho.mqtt.client
import sys
import time

servoChannel = 1
pload = 2


def setServo(servoChannel, position):
        servoStr = "%u=%u\n" % (servoChannel, position)
        with open("/dev/servoblaster", "wb") as f:
                f.write(servoStr)


def on_connect(client, userdata, flags, rc):
        print('connected (%s)' % client._client_id)
        client.subscribe(topic='lightinggreen', qos=2)


def on_message(client, userdata, message):
        print('------------------------------')
        print('topic: %s' % message.topic)
        print('payload: %s' % message.payload)
        print('qos: %d' % message.qos)
        if (int(message.payload) == 1):
                setServo(servoChannel, 100)
                print("led on")
        elif (int(message.payload) == 0):
                setServo(servoChannel, -100)
                print("led off")
        else:
                print("not a string")


if __name__ == '__main__':
        username = sys.argv[1]
        password = sys.argv[2]
        client = paho.mqtt.client.Client(client_id='hu2019_uhack', clean_session = False)
        client.username_pw_set(username, password)
        client.on_connect=on_connect
        client.on_message=on_message
        client.connect(host='sandbox.rightech.io', port=1883)
        client.loop_forever()
