#! usr/bin/python3
import sys
import paho.mqtt.client
import paho.mqtt.publish
import serial
from time import sleep

speed = 9600


def on_connect(client, userdata, flags, rc):
    print('connected')


def publish_message(pload, topic_name, username, password):
    paho.mqtt.publish.single(
        topic=topic_name,
        payload=pload,
        qos=2,
        hostname='sandbox.rightech.io',
        port=1883,
        client_id='hu2019_uhack',
        auth={
            'username': username,
            'password': password
        }
    )


def parse(light, temp, msg):
    param_list = msg.split("_")
    print(param_list)
    light = int(param_list[0])
    temp = int(param_list[1])


if __name__ == "__main__":
    username = sys.argv[1]
    password = sys.argv[2]
    port = '/dev/ttyACM0'
    serial_connection = serial.Serial(port, speed)
    t = 'temp'
    temp = 100
    l = 'light'
    light = 9
    topic_name = 'sens'

    while True:
        msg = serial_connection.readline().decode('UTF-8')
        param_list = msg.split("_")
        print(param_list)
        light = int(param_list[0])
        temp = int(param_list[1])
        print(light)
        print(temp)

        pload = '{"%s":"%d", "%s":"%d"}' % (t, temp, l, light)
        publish_message(pload, topic_name, username, password)


# client = paho.mqtt.client.Client(client_id='hu2019_uhack', clean_session=False)
# client.username_pw_set('111', '111')
# client.on_connect = on_connect
# client.on_message = on_message

# client.connect(host='sandbox.rightech.io', port=1883)
# client.loop_forever()
